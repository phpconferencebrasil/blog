<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'blog');
// Project repository
set('repository', 'git@gitlab.com:phpconferencebrasil/blog.git');
// Shared files/dirs between deploys 
add('shared_files', ['.env']);
add('shared_dirs', []);
// Writable dirs by web server 
add('writable_dirs', []);


host('34.95.193.224')
    ->set('deploy_path', '/var/www/html/{{application}}')
    ->user('honjoya')
    ->roles('web1', 'web')
    ->identityFile(
    	'./deploy/ssh/id_rsa', 
    	'./deploy/ssh/id_rsa.pub'
    );    

task('artisan:optimize', function () {});

task('reload:php-fpm', function () {
    run('sudo /etc/init.d/php7.2-fpm reload');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

after('deploy', 'reload:php-fpm');
after('rollback', 'reload:php-fpm');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');